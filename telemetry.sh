#!/bin/bash                                                                     

## tcp port 2003 is graphite db

while true ; do
    tail -n +1 -F udpeq.telemetry.log | tee >(cat 1>&2) | socat -t 0 - TCP:localhost:2003
    sleep 1
done
