/*
    UDPEQ
    UDP load balancing over parallel routes
    Copyright (C) 2001..2022 Truxton Fulton <udpeq@truxton.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
*/

#define UDPEQ_VERSION "1.006"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <assert.h>
#include <pthread.h>
#include <semaphore.h>
#include <poll.h>

#define STRLEN 8192
#define MAX_BUFLEN 32768
#define OUT_BUFFER_SIZE 32
// #define OUT_BUFFER_SIZE 128
// #define OUT_BUFFER_SIZE 512
#define ASSEMBLY_BUFFER_SIZE 8192
#define LATE_BUFFER_SIZE 65536 /* must be an even number, and at least twice as large as ASSEMBLY_BUFFER_SIZE */

// #define UDPEQ_HEADER_SIZE ((2*sizeof(int))+(3*sizeof(long long))+(1*sizeof(char)))  /* 33 -- subtract this from the link MTU */
#define UDPEQ_HEADER_SIZE ((2*sizeof(int))+(3*sizeof(long long))+(3*sizeof(char)))  /* 35 -- subtract this from the link MTU */
#define UDPEQ_DATA_MAGIC 0xDE1BFE1F
#define UDPEQ_META_MAGIC 0x04206969
#define UDP_SEND_SOCKBUFSIZE 65536
#define DEFAULT_DEBUG 3
// #define DEFAULT_DEBUG 7

// #define MAX_WAIT_FOR_MISSING_PACKET_USEC 250000
// #define MAX_WAIT_FOR_MISSING_PACKET_USEC 25000  // when this is too high, then it adds cumulative delay for lossy channels
// #define MAX_WAIT_FOR_MISSING_PACKET_USEC 3000
// #define MAX_WAIT_FOR_MISSING_PACKET_USEC 8000
#define MAX_WAIT_FOR_MISSING_PACKET_USEC 1000

int DEBUG=DEFAULT_DEBUG ;
long long global_sequence_counter=0 ;
struct timeval last_LO_send_time ;
FILE *debug_log=NULL ;
FILE *telemetry_log=NULL ;
long long debug_lines=0 ;
long long inbuffer_signal_counter=0 ;
long long inbuffer_signal_timeout=0 ;

struct udpeq_packet
{
  char *buf ;
  char channel ;
  struct timeval recv_time ;
  struct timeval send_time ;
  int buflen ;
  long long sequence_number ;
  int refcount ;  // used when packet is duplicated among multiple channels' outbuffer
  struct udpeq_packet *newer ;  // for use in latebuffer linked list
  struct udpeq_packet *older ;  // for use in latebuffer linked list
  long long inbuffer_signal_counter ;
  long long inbuffer_signal_timeout ;
} ;

// main assembly buffer
struct udpeq_packet **inbuffer=NULL ;
int inbuffer_size=0 ;
int inbuffer_first=0 ;  // index of first array slot
sem_t inbuffer_mutex ;
sem_t inbuffer_signal ;
long long inbuffer_sequence=0 ;  // sequence number corresponding to the "first" slot of inbuffer
long long inbuffer_last=0 ;  // sequence number corresponding to the "last" slot of inbuffer

// keep track of packets that arrive too-late.  we may deliver them, but need to avoid duplicates
sem_t latebuffer_mutex ;
struct udpeq_packet *latebuffer_oldest=NULL ;
struct udpeq_packet *latebuffer_newest=NULL ;
long long *latebufferA=NULL ;
long long *latebufferB=NULL ;
long long latebufferA_0=0 ;
long long latebufferB_0=LATE_BUFFER_SIZE/2 ;

sem_t refcount_mutex ;
sem_t history_slot_mutex ;

enum udpeq_channel_type
{
  LI,  /* local input */
  LO,  /* local output */
  RI,  /* remote input */
  RO,  /* remote output */
  UNKNOWN
} ;

struct udpeq_channel_history
{
  long long min_packet_number ;
  long long max_packet_number ;
  long long packet_count ;  // per history slot
  long long packet_counter ;  // global counter
  long long byte_count ;
  long long num_late ;
  long long num_soon ;
  long long num_jumpstarts ;
  long long remote_dropped ;
  double accumulated_weighted_use ;
  double accumulated_latency_total ;
  double accumulated_latency_network ;
  double accumulated_latency_buffer ;
} ;
#define HISTORY_SLOT_DURATION_SECS 12
#define NUM_HISTORY_SLOTS 300

/* 1 in 256 chance to emit spammy info */
#define RANDOM_INFO_PERIOD 256

/* consider packet dropped over the last three history slots */
#define NUM_HISTORY_SLOTS_FOR_NEAR_DROP_RATE 3
#define NUM_HISTORY_SLOTS_FOR_FAR_DROP_RATE 256
#define NEAR_DROP_RATE_THRESHOLD 0.20
#define FAR_DROP_RATE_THRESHOLD 0.02

struct udpeq_channel
{
  enum udpeq_channel_type type ;
  char *name ;  // overall name of channel
  char *destname ;  // name of destination address (different if passive return channel)
  char *hostname ;
  int port ;
  int local_port ;
  in_addr_t local_address ;  /* address of local interface */
  char *local_iface ;

  struct udpeq_channel_history history[NUM_HISTORY_SLOTS] ;
  int last_history_slot ;

  long long packet_count ;
  long long num_jumpstarts ;
  double weight_usage ;  // external constraint (e.g. metered usage)
  double weight_capacity ;  // adapts to bandwidth
  double weight_temp ;

  double near_drop_rate ;
  double far_drop_rate ;
  long long near_dropped ;
  long long far_dropped ;
  long long near_total ;
  long long far_total ;

  int debug_loss_flag ;

  int min_network_delta_usec ;

  sem_t outbuffer_signal ;
  struct udpeq_packet **outbuffer ;
  int outbuffer_size ;
  int outbuffer_first ;
  int outbuffer_len ;
  sem_t outbuffer_mutex ;


  /* a master/slave pair share the same fd */
  /* master_channel and slave_channel cannot both be non-NULL */
  /* in the case of this being a passive reply channel, else NULL */
  struct udpeq_channel *master_channel ;
  /* reverse link, NULL if replies on this channel are ignored */
  struct udpeq_channel *slave_channel ;

  double latency ;  /* estimate of current latency (usec) */
  double throughput ;  /* current estimate of throughput (bytes per usec) */
  double packet_loss ;  /* estimate of fraction of packets lost [0..1] */

  char slave_reply_address[128] ;
  int fd ;
  struct sockaddr_in sa ;
  struct sockaddr_in lsa ;
  int active ;
  int whofrom_init ;
  int ref_flag ;
  char symbol ;

  char remote_symbol ;
  long long remote_packet_count ;

  int max_recv_size ;
  int max_send_size ;

  pthread_t channel_thread ;

  struct udpeq_channel *previous ;
} ;

struct udpeq_channel *channel_tail=NULL ;

char *name_of_type(struct udpeq_channel *c, int pad, int simplify)
{
  char *result=NULL ;
  switch(c->type)
    {
    case LI :
      if(c->master_channel!=NULL)
        {
          if(simplify)
            result="LI" ;
          else
            result="LI(psv)" ;
        }
      else
	if(c->slave_channel!=NULL)
          {
            if(pad)
              result="LIO    " ;
            else
              result="LIO" ;
          }
	else
          {
            if(pad)
              result="LI     " ;
            else
              result="LI" ;
          }
      break ;;;
    case LO :
      if(c->master_channel!=NULL)
	result="LO(psv)" ;
      else
	if(c->slave_channel!=NULL)
          {
            if(pad)
              result="LOI    " ;
            else
              result="LOI" ;
          }
	else
          {
            if(pad)
              result="LO     " ;
            else
              result="LO" ;
          }
      break ;;;
    case RI :
      if(c->master_channel!=NULL)
	result="RI(psv)" ;
      else
	if(c->slave_channel!=NULL)
          {
            if(pad)
              result="RIO    " ;
            else
              result="RIO" ;
          }
	else
          {
            if(pad)
              result="RI     " ;
            else
              result="RI" ;
          }
      break ;;;
    case RO :
      if(c->master_channel!=NULL)
        {
          if(simplify)
            result="RO" ;
          else
            result="RO(psv)" ;
        }
      else
	if(c->slave_channel!=NULL)
          {
            if(pad)
              result="ROI    " ;
            else
              result="ROI" ;
          }
	else
          {
            if(pad)
              result="RO     " ;
            else
              result="RO" ;
          }
      break ;;;
    default :
      result="UNKNOWN" ;
      break ;;;
    }
  return(result) ;
}

void mutex_take(sem_t *mutex)
{
  int ret ;

  do
    ret=sem_wait(mutex) ;
  while((ret<0)&&(errno==EINTR)) ;
}

int mutex_take_timed(sem_t *mutex,int usec)
{
  int ret ;
  struct timespec timeout ;
  struct timeval now ;

  timeout.tv_sec=(time_t) usec/1000000 ;
  timeout.tv_nsec=(long) ((usec%1000000)*1000) ;

  gettimeofday(&now,NULL) ;

  timeout.tv_sec += now.tv_sec ;
  timeout.tv_nsec += (now.tv_usec * 1000) ;
  for(;timeout.tv_nsec>1000000000;)
    {
      timeout.tv_nsec-=1000000000 ;
      timeout.tv_sec+=1 ;
    }
  
  do
    ret=sem_timedwait(mutex,&timeout) ;
  while((ret<0)&&(errno==EINTR)) ;
  if((ret<0) && (DEBUG>7))
    {
      perror("sem_timedwait") ;
      fprintf(stderr,"ret=%d, usec=%d, errno=%d, tv_sec=%ld, tv_nsec=%ld\n",ret,usec,errno,timeout.tv_sec,timeout.tv_nsec) ;
    }
  return(ret) ;
}

void mutex_give(sem_t *mutex)
{
  int ret ;

  do
    ret=sem_post(mutex) ;
  while((ret<0)&&(errno==EINTR)) ;
}

void usage(char *program_name)
{
  fprintf(stderr," \n\
---------------- USAGE: ---------------- \n\
%s -LI host:port -LO host:port [[-name XXX] -RI [localaddress]:port[:localaddress:localport]]... \n\
   [[-name XXX] -RO host:port[:localaddress:localport]][,usage_weight[,capacity_weight]]... \n\
   [-v debug_level] \n\
   [-t] \n\
 \n\
You must specify exactly one LI and one LO option. \n\
You must specify at least one RI and one RO option. \n\
LI means local input (talking to local vpn process). \n\
LO means local output (talking to local vpn process). \n\
RI means remote input (talking to remote udpeq process). \n\
RO means remote output (talking to remote udpeq process). \n\
RIO means remote input with reply output. \n\
ROI means remote output with reply input. \n\
LIO means local input with reply output. \n\
LOI means local output with reply input. \n\
weight is an optional parameter for RO channels between 0.0 and 1.0. \n\
prepending the \"-name XXX\" option before -RI or -RO will associate a user-defined name with the channel. \n\
\n\
udpeq listens on LI for packets and encapsulates them \n\
over an optimized equalized tunnel to a remote udpeq. \n\
The encapsulated packets may travel over any of the \n\
RO sockets.  Return encapsulated packets are gathered \n\
at the RI sockets, unencapsulated, and output to LO. \n\
LI and LO may be the same port, similarly for R[IO]. \n\
The host name may be omitted to indicate localhost. \n\
Instead of localaddress you may specify \"if=ifname\" \n\
  For example : -RIO :1234:if=ppp1:5678 \n\
-t saves timeseries data to udpeq.telemetry.log (graphite data format) \n\
On top of udpeq you should run wireguard or openvpn. \n\
",program_name) ;
}

struct udpeq_channel *parse_command_line(int argc, char **argv)
{
  struct udpeq_channel *result=NULL,*c,*master_channel ;
  enum udpeq_channel_type type, reply_channel ;
  int li=0,lo=0,ri=0,ro=0,fatal=0 ;
  int local_port ;
  struct timeval now ;
  char symbol='A' ;
  char *colon0=NULL, *colon1=NULL, *colon2=NULL, *comma, *comma2 ;
  char *ifaceeq=NULL ;
  int i,j,k,ix,meta_arg,ret ;
  char *p ;
  char *name=NULL ;
  char *name_arg=NULL ;

  gettimeofday(&now,NULL) ;
  for(i=1;i<argc;i++)
    {
      meta_arg=0 ;
      reply_channel=UNKNOWN ;
      if(!strcmp(argv[i],"-name"))
	{
          meta_arg=1 ;
          name_arg=strdup(argv[++i]) ;
	}
      if(!strcmp(argv[i],"-LI"))
	{
	  type=LI ;
	  li++ ;
	}
      if(!strcmp(argv[i],"-LO"))
	{
	  type=LO ;
	  lo++ ;
	}
      if(!strcmp(argv[i],"-RI"))
	{
	  type=RI ;
	  ri++ ;
	}
      if(!strcmp(argv[i],"-RO"))
	{
	  type=RO ;
	  ro++ ;
	}
      if(!strcmp(argv[i],"-RIO"))
	{
	  type=RI ;
	  reply_channel = RO ;
	  ri++ ;
	  ro++ ;
	}
      if(!strcmp(argv[i],"-ROI"))
	{
	  type=RO ;
	  reply_channel = RI ;
	  ro++ ;
	  ri++ ;
	}
      if(!strcmp(argv[i],"-LIO"))
	{
	  type=LI ;
	  reply_channel = LO ;
	  li++ ;
	  lo++ ;
	}
      if(!strcmp(argv[i],"-LOI"))
	{
	  type=LO ;
	  reply_channel = LI ;
	  lo++ ;
	  li++ ;
	}
      if(!strncmp(argv[i],"-v",strlen("-v")))
        {
          debug_log=fopen("udpeq.debug.log","a") ;
          if(debug_log)
            fprintf(debug_log,"---------------------- UDPEQ %s ----------------------\n",UDPEQ_VERSION) ;

          meta_arg=1 ;
          if((strlen(argv[i])==strlen("-v")) && (i+1<argc))
            p=argv[++i] ;
          else
            p=argv[i]+strlen("-v") ;
          if(1!=sscanf(p,"%d",&DEBUG))
            {
              fprintf(stderr,"%s ERROR: -v needs a verbosity level (default is %d)\n",argv[0],DEFAULT_DEBUG) ;
              fatal=1 ;
            }
        }
      if(!strncmp(argv[i],"-t",strlen("-t")))
        {
          telemetry_log=fopen("udpeq.telemetry.log","a") ;
          meta_arg=1 ;
        }

      if(meta_arg)
        {
          /* dont do anything with building the node list */
        }
      else
        {
          if(type==UNKNOWN)
            {
              fprintf(stderr,"%s ERROR: unknown option \"%s\"\n",
                      argv[0],argv[i]) ;
              fatal=1 ;
            }
          else
            {
              i++ ;
              if(i==argc)
                {
                  fprintf(stderr,"%s ERROR: %s missing socket specification\n",
                          argv[0],argv[i-1]) ;
                  fatal=1 ;
                }
              else
                {
                  colon0=strstr(argv[i],":") ;
                  if(colon0==NULL)
                    {
                      fprintf(stderr,"%s ERROR: invalid socket specification \"%s\"\n",argv[0],argv[i]) ;
                      fatal=1 ;
                    }
                  else
                    {
                      comma2=NULL ;
                      comma=strstr(colon0,",") ;
                      if(comma)
                        {
                          *comma=0 ;
                          comma2=strstr(comma+1,",") ;
                          if(comma2)
                            *comma2=0 ;
                        }
                      *colon0=0 ;
                      colon1=strstr(colon0+1,":") ;
                      if(colon1)
                        *colon1=0 ;
                      if(colon1==NULL)
                        colon2=NULL ;
                      else
                        colon2=strstr(colon1+1,":") ;
                      if(colon2)
                        *colon2=0 ;

                      /* specifying interface device instead of local ip address */
                      if(colon1)
                        {
                          ifaceeq=strstr(colon1+1,"if=") ;
                          if(ifaceeq)
                            {
                              *ifaceeq=0 ;
                              ifaceeq+=3 ;
                            }
                        }

                      name=name_arg ;
                      name_arg=NULL ;

                      master_channel=NULL ;
                      for(;type!=UNKNOWN;)
                        {
                          c=(struct udpeq_channel *) calloc(1,sizeof(struct udpeq_channel)) ;
                          c->type=type ;
                          c->name=name ;
                          c->hostname=strdup(argv[i]) ;
                          c->packet_count=0 ;
                          c->num_jumpstarts=0 ;
                          c->last_history_slot= -1 ;
                          c->min_network_delta_usec=0 ;
                          ret=sem_init(&(c->outbuffer_signal), 0, 1) ;
                          ret=sem_init(&(c->outbuffer_mutex), 0, 1) ;
                          c->outbuffer=(struct udpeq_packet **)calloc(ASSEMBLY_BUFFER_SIZE,sizeof(struct udpeq_packet *)) ;
                          c->outbuffer_size=OUT_BUFFER_SIZE ;
                          c->outbuffer_first=0 ;
                          c->outbuffer_len=0 ;

                          for(ix=0;ix<NUM_HISTORY_SLOTS;ix++)
                            memset(&(c->history[ix]),0,sizeof(struct udpeq_channel_history)) ;

                          if((type==RO)&&(comma))
                            {
                              sscanf(comma+1,"%lf",&(c->weight_usage)) ;
                            }
                          else
                            c->weight_usage=1.0 ;

                          if((type==RO)&&(comma2))
                            {
                              sscanf(comma2+1,"%lf",&(c->weight_capacity)) ;
                            }
                          else
                            c->weight_capacity=1.0 ;

                          if(strlen(c->hostname)<1)
                            {
                              free(c->hostname) ;
                              if((type==LI)||(type==RI))
                                c->hostname=strdup("0.0.0.0") ;
                              else  /* output socket */
                                c->hostname=strdup("127.0.0.1") ;
                            }
                          c->port=atoi(colon0+1) ;
                          if(colon1)
                            {
                              if(!ifaceeq)
                                c->local_address=inet_addr(colon1+1) ;
                              else
                                {
                                  c->local_address=INADDR_ANY ;
                                  c->local_iface=strdup(ifaceeq) ;
                                }
                            }
                          else
                            c->local_address=INADDR_ANY ;

                          if(colon2)
                            c->local_port=atoi(colon2+1) ;
                          else
                            if(type==RO)
                              c->local_port=0 ;
                            else
                              c->local_port=c->port ;

                          c->master_channel=master_channel ;
                          if(master_channel)
                            master_channel->slave_channel=c ;
                          c->fd= -1 ;
                          c->active=0 ;
                          c->whofrom_init=0 ;
                          c->previous=result ;
                          c->symbol=symbol++ ;
                          
                          c->remote_symbol='?' ;
                          c->remote_packet_count=0 ;

                          c->max_recv_size=0 ;
                          c->max_send_size=0 ;

                          c->latency=0.0 ;
                          c->throughput=0.0 ;
                          c->packet_loss=0.0 ;
                          
                          result=c ;
                          type=reply_channel ;
                          if(type!=UNKNOWN)
                            {
                              reply_channel=UNKNOWN ;
                              master_channel=c ;
                            }
                        }
                    }
                }
            }
	}
    }

  if(li!=1)
    {
      fprintf(stderr,"%s: ERROR: you must specify exactly one -LI option\n",
              argv[0]) ;
      fatal=1 ;
    }
  if(lo!=1)
    {
      fprintf(stderr,"%s: ERROR: you must specify exactly one -LO option\n",
              argv[0]) ;
      fatal=1 ;
    }
  if(ri<1)
    {
      fprintf(stderr,"%s: ERROR: you must specify at least one -RI option\n",
              argv[0]) ;
      fatal=1 ;
    }
  if(ro<1)
    {
      fprintf(stderr,"%s: ERROR: you must specify at least one -RO option\n",
              argv[0]) ;
      fatal=1 ;
    }

  if(fatal)
    result=NULL ;

  /* packets sent to LO should have return address of LI */
  for(c=result;c!=NULL;c=c->previous)
    if(c->type==LI)
      local_port=c->port ;
  for(c=result;c!=NULL;c=c->previous)
    if(c->type==LO)
      c->local_port=local_port ;

  return(result) ;
}


void close_channel(struct udpeq_channel *c)
{
  int err ;

  err=0 ;

  if(c==NULL)
    err=1 ;

  if(!err)
    {
      if(c->fd>=0)
	close(c->fd) ;
      c->fd= -1 ;
      c->active=0 ;
      c->whofrom_init=0 ;
      if(c->master_channel)
	{
	  if(DEBUG>4)
	    fprintf(stderr,"\nclosing master channel of slave %c : %s (%s:%d)  ", c->symbol,name_of_type(c,1,0),c->hostname,c->port) ;
	  c->master_channel->fd= -1 ;
          c->master_channel->whofrom_init=0 ;
          c->master_channel->active=0 ;
	}

      if(c->slave_channel)
	{
	  if(DEBUG>4)
	    fprintf(stderr,"\nclosing slave channel of master %c : %s (%s:%d)  ", c->symbol,name_of_type(c,1,0),c->hostname,c->port) ;
	  c->slave_channel->fd= -1 ;
          c->slave_channel->whofrom_init=0 ;
          c->slave_channel->active=0 ;
	}
    }
}

int get_addrval(char *hostname, unsigned long int *addrval)
{
  int err=0 ;
  char hostip[STRLEN] ;
  char message[STRLEN] ;
  struct hostent *hostaddr ;
  
  hostip[0]=0 ;
  
  /* if user gave us an IP address */
  if((*addrval = inet_addr(hostname)) != -1)
    {
      sprintf(hostip,"%s",hostname) ;
    }
  else  /* resolve hostname */
    {
      hostaddr=gethostbyname(hostname) ;
      if(hostaddr!=NULL)
        {
          sprintf(hostip,"%d.%d.%d.%d",
                  (((unsigned char *)
                    (hostaddr->h_addr_list[0]))[0]),
                  (((unsigned char *)
                    (hostaddr->h_addr_list[0]))[1]),
                  (((unsigned char *)
                    (hostaddr->h_addr_list[0]))[2]),
                  (((unsigned char *)
                    (hostaddr->h_addr_list[0]))[3])) ;
        }
      else
        {
          int ipb0, ipb1, ipb2, ipb3 ;
          if(4==sscanf(hostname,"%d.%d.%d.%d",
                       &ipb0,&ipb1,&ipb2,&ipb3))
            {
              sprintf(hostip,"%d.%d.%d.%d",ipb0,ipb1,ipb2,ipb3) ;
              printf("get_addrval: cannot resolve host name,"
                     " trying literal IP address %s\n",hostip) ;
            }
        }
      if(hostip[0]==0)
        {
          fprintf(stderr,"get_addrval: cannot resolve host name"
                  " : %s\n",hostname) ;
          err=1 ;
        }
      else
        if((*addrval = inet_addr(hostip)) == -1)
          {
            fprintf(stderr,"get_addrval: invalid host IP"
                    " : %s\n",hostname) ;
            err=1 ;
          }
    }
  return(err) ;
}

int open_channel(struct udpeq_channel *c)
{
  int err, opt ;
  unsigned long int remote_addrval ;
  unsigned long int local_addrval ;
  char hostip[STRLEN] ;
  char temps[2*STRLEN] ;

  err=0 ;

  if(c==NULL)
    err=1 ;

  if(!err)
    {
      if(c->master_channel)
	{
	  if(c->master_channel->fd<0)
	    open_channel(c->master_channel) ;
	  c->fd=c->master_channel->fd ;
	}
      else
	{
	  if(DEBUG>3)
	    fprintf(stderr,"\nopening %c: %s socket (%s:%d) from port %d...\n", c->symbol,name_of_type(c,1,0),c->hostname, c->port,c->local_port) ;
	  c->fd=socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP) ;
	  if(c->fd<0)
	    {
	      perror("socket") ;
	      err=1 ;
	    }
	  if(!err)
	    {
	      opt = 1 ;
	      if(setsockopt(c->fd, SOL_SOCKET, SO_REUSEADDR,(char *) &opt, sizeof(opt))==-1)
		{
		  perror("setsockopt (1)") ;
		  close(c->fd) ;
		  c->fd= -1 ;
		  err=1 ;
		}
	    }
	  if(!err)
	    {
	      opt = UDP_SEND_SOCKBUFSIZE ;
	      if(setsockopt(c->fd, SOL_SOCKET, SO_SNDBUF,(char *) &opt, sizeof(opt))==-1)
		{
		  perror("setsockopt (1)") ;
		  close(c->fd) ;
		  c->fd= -1 ;
		  err=1 ;
		}
	    }
	  
	  if(!err)
            {
              err=get_addrval(c->hostname,&remote_addrval) ;

              if(err)
                {
                  close(c->fd) ;
                  c->fd= -1 ;
                }
            }
	}
    }

  if(((c->type==RO) || (c->type==LO)) && (c->master_channel) && (!(c->whofrom_init)))
    {
      fprintf(stderr,"[%s] passive outputs need to be opened once some input has been received on the master channel\n",c->name) ;
      err=1 ;
    }

  if(c->type==RI)
    {
      c->remote_symbol='?' ;
      c->remote_packet_count=0 ;
    }

  c->max_recv_size=0 ;
  c->max_send_size=0 ; 
      
  if(!err)
    {
      if(((c->type==LI)||(c->type==RI)) && (c->master_channel==NULL))
	{
	  c->sa.sin_family = AF_INET ;
	  c->sa.sin_addr.s_addr = INADDR_ANY ;
	  c->sa.sin_port = htons(c->local_port) ;
          strcpy(hostip,inet_ntoa(inet_makeaddr(c->sa.sin_addr.s_addr,c->sa.sin_addr.s_addr))) ;
          sprintf(temps,"%s:%d",hostip,c->local_port) ;
          if(c->destname)
            free(c->destname) ;
          c->destname=strdup(temps) ;
	  if(bind(c->fd,(struct sockaddr *) &c->sa,
                  sizeof(struct sockaddr_in))<0)
	    {
	      if(DEBUG>0)
		{
		  fprintf(stderr,"ERROR binding %s socket (%s) fd==%d for channel %c\n",name_of_type(c,1,0),c->destname,c->fd,c->symbol) ;
		  perror("open_channel: bind") ;
		}
	    }
          else
            {
	      if(DEBUG>0)
		{
                  strcpy(hostip,inet_ntoa(inet_makeaddr(c->sa.sin_addr.s_addr,c->sa.sin_addr.s_addr))) ;
		  fprintf(stderr,"SUCCESS binding %s socket (%s:%d) fd==%d for channel %c\n",name_of_type(c,1,0),hostip,c->local_port,c->fd,c->symbol) ;
		}
            }
	}

      if(((c->type==LO) || (c->type==RO)) && (c->master_channel) && (c->whofrom_init))
        {
          if(c->destname)
            free(c->destname) ;
          c->destname=NULL ;
          // strcpy(hostip,inet_ntoa(inet_makeaddr(c->sa.sin_addr.s_addr,c->sa.sin_addr.s_addr))) ;
          strcpy(hostip,inet_ntoa(c->sa.sin_addr)) ;
          if(connect(c->fd,(struct sockaddr*) &c->sa,sizeof(struct sockaddr_in))<0)
            {
              if(DEBUG>0)
                {
                  perror("open_channel: slave reverse connect") ;
                  fprintf(stderr,"error connecting %s socket (%s:%d) fd=%d\n", name_of_type(c,1,0),c->hostname,c->port,c->fd) ;
                }
              c->whofrom_init=0 ;
              err=1 ;
            }
          else
            {
              sprintf(temps,"%s:%d",hostip,c->local_port) ;
              c->destname=strdup(temps) ;
            }
        }

      if(((c->type==LO) || (c->type==RO)) && (c->master_channel==NULL))
	{
          if((c->type==RO) && (c->local_port>0))
            {
              c->lsa.sin_family = AF_INET ;
              c->lsa.sin_addr.s_addr = INADDR_ANY ;
              c->lsa.sin_port = htons(c->local_port) ;
              if(bind(c->fd,(struct sockaddr *) &c->lsa,
                      sizeof(struct sockaddr_in))<0)
                {
                  if(DEBUG>0)
                    {
                      strcpy(hostip,inet_ntoa(inet_makeaddr(c->lsa.sin_addr.s_addr,c->lsa.sin_addr.s_addr))) ;
                      fprintf(stderr,"error binding %s socket (%s:%d) fd=%d\n",
                              name_of_type(c,1,0),hostip,c->local_port,c->fd) ;
                      perror("open_channel: bind") ;
                    }
                  close_channel(c) ;
                  err=1 ;
                }
              else
                {
                  if(DEBUG>0)
                    {
                      strcpy(hostip,inet_ntoa(inet_makeaddr(c->lsa.sin_addr.s_addr,c->lsa.sin_addr.s_addr))) ;
                      fprintf(stderr,"success binding %s socket (%s:%d) fd=%d for channel %c\n",name_of_type(c,1,0),hostip,c->local_port,c->fd, c->symbol) ;
                    }
                }
            }
          if(!err)
            {
              c->sa.sin_family = AF_INET ;
              c->sa.sin_addr.s_addr = remote_addrval ;
              c->sa.sin_port = htons(c->port) ;

              if(DEBUG>2)
                {
                  fprintf(stderr,"connecting %s fd=%d\n", c->name,c->fd) ;
                }

              if(connect(c->fd,(struct sockaddr*) &c->sa,sizeof(struct sockaddr_in))<0)
                {
                  if(DEBUG>0)
                    {
                      perror("open_channel: connect") ;
                      fprintf(stderr,"error connecting %s socket (%s:%d) fd=%d\n", name_of_type(c,1,0),c->hostname,c->port,c->fd) ;
                    }
                  close_channel(c) ;
                  err=1 ;
                }
            }
	}
    }

  if(!err)
    {
      c->active=1 ;
      if(DEBUG>5)
	fprintf(stderr,"successfully opened %s socket (%s:%d) (%c)\n",name_of_type(c,1,0),c->hostname,c->port,c->symbol) ;
    }
  return(err) ;
}

void reset_latebuffer(long long seq)
{
  mutex_take(&latebuffer_mutex) ;
  if((seq<latebufferA_0)&&(seq<latebufferB_0))
    {
      latebufferA_0=(seq / LATE_BUFFER_SIZE) * LATE_BUFFER_SIZE ;
      latebufferB_0=latebufferA_0 + (LATE_BUFFER_SIZE/2) ;
    }
  mutex_give(&latebuffer_mutex) ;
}

int update_latebuffer(long long seq)
{
  int result=0 ;
  long long offsA, offsB ;

  mutex_take(&latebuffer_mutex) ;
  if((seq<latebufferA_0)&&(seq<latebufferB_0))
    {
      result=1 ;
      if(DEBUG>5)
        fprintf(stderr,"dropping late-late packet with seq=%lld < (%lld or %lld)\n",seq,latebufferA_0,latebufferB_0) ;
    }
  else
    {
      // catch up to recent sequence
      for(;(seq>=latebufferA_0+LATE_BUFFER_SIZE)&&(seq>=latebufferB_0+LATE_BUFFER_SIZE);)
        if(latebufferA_0 < latebufferB_0)
          latebufferA_0+=LATE_BUFFER_SIZE ;
        else
          latebufferB_0+=LATE_BUFFER_SIZE ;

      offsA=seq-latebufferA_0 ;
      offsB=seq-latebufferB_0 ;
      
      if((offsA>=0)&&(offsA<LATE_BUFFER_SIZE))
        {
          if(latebufferA[offsA]==seq)
            result=2 ;
          else
            latebufferA[offsA]=seq ;
        }
      if((offsB>=0)&&(offsB<LATE_BUFFER_SIZE))
        {
          if(latebufferB[offsB]==seq)
            result=2 ;
          else
            latebufferB[offsB]=seq ;
        }
    }
  mutex_give(&latebuffer_mutex) ;
  return(result) ;
}


int rotate_history_slots(struct timeval *now, struct udpeq_channel *chan)
{
  int result, previous_history_slot, near_earlier_history_slot, far_earlier_history_slot ;
  long long near_dropped, far_dropped, near_total, far_total ;
  int random_verbose=0 ;

  result=(now->tv_sec / HISTORY_SLOT_DURATION_SECS) % NUM_HISTORY_SLOTS ;

  if(chan)
    {
      mutex_take(&history_slot_mutex) ;
      if(result != chan->last_history_slot)
        {
          previous_history_slot=chan->last_history_slot ;
          chan->last_history_slot = result ;

          if(chan->debug_loss_flag)
            {
              chan->debug_loss_flag=0 ;
              random_verbose=1 ;
              fprintf(stderr,"rotate_history_slots {%c} DEBUG LOSS FLAG [%d] prev [%d] =================================\n",chan->symbol,result,chan->last_history_slot) ;
            }

          if(previous_history_slot>=0)
            {
              chan->history[previous_history_slot].remote_dropped=chan->packet_count - chan->remote_packet_count ;
              if(chan->remote_packet_count < chan->packet_count)
                chan->history[previous_history_slot].packet_counter=chan->packet_count ;
              else
                chan->history[previous_history_slot].packet_counter=chan->remote_packet_count ;
            }
          previous_history_slot=(result + NUM_HISTORY_SLOTS - 1) % NUM_HISTORY_SLOTS ;
          near_earlier_history_slot=(result + NUM_HISTORY_SLOTS - (1 + NUM_HISTORY_SLOTS_FOR_NEAR_DROP_RATE)) % NUM_HISTORY_SLOTS ;
          far_earlier_history_slot=(result + NUM_HISTORY_SLOTS - (1 + NUM_HISTORY_SLOTS_FOR_FAR_DROP_RATE)) % NUM_HISTORY_SLOTS ;

          near_dropped=llabs(chan->history[previous_history_slot].remote_dropped - chan->history[near_earlier_history_slot].remote_dropped) ;
          far_dropped=llabs(chan->history[previous_history_slot].remote_dropped - chan->history[far_earlier_history_slot].remote_dropped) ;

          near_total=llabs(chan->history[previous_history_slot].packet_counter - chan->history[near_earlier_history_slot].packet_counter) ;
          far_total=llabs(chan->history[previous_history_slot].packet_counter - chan->history[far_earlier_history_slot].packet_counter) ;

          chan->near_dropped=near_dropped ;
          chan->far_dropped=far_dropped ;

          chan->near_total=near_total ;
          chan->far_total=far_total ;

          if(near_total>0)
            chan->near_drop_rate=((double) near_dropped)/((double) near_total) ;
          if(chan->near_drop_rate>1.0)
            chan->near_drop_rate=1.0 ;

          if(far_total>0)
            chan->far_drop_rate=((double) far_dropped)/((double) far_total) ;
          if(chan->far_drop_rate>1.0)
            chan->far_drop_rate=1.0 ;

          if(random_verbose)
            fprintf(stderr,"rotate_history_slots {%c} [%d] NEAR drop_rate=%g (%lld/%lld) FAR drop_rate=%g (%lld/%lld) \n",chan->symbol,result,chan->near_drop_rate,near_dropped,near_total,chan->far_drop_rate,far_dropped,far_total) ;

          memset(&(chan->history[result]),0,sizeof(struct udpeq_channel_history)) ;
        }
      mutex_give(&history_slot_mutex) ;
    }
  return(result) ;
}

void update_history_slot(struct udpeq_channel *chan, struct udpeq_packet *packet)
{
  struct timeval t1,now ;
  int this_history_slot ;

  gettimeofday(&now,NULL) ;
  this_history_slot=rotate_history_slots(&now,chan) ;
  if((chan->history[this_history_slot].min_packet_number > packet->sequence_number) ||
     (chan->history[this_history_slot].packet_count == 0))
    chan->history[this_history_slot].min_packet_number = packet->sequence_number ;
  if(chan->history[this_history_slot].max_packet_number < packet->sequence_number)
    chan->history[this_history_slot].max_packet_number = packet->sequence_number ;
  chan->history[this_history_slot].packet_count++ ;
  chan->history[this_history_slot].byte_count+=packet->buflen ;
}

void *udpeq_channel_thread(char *name, struct udpeq_channel *chan)
{
  struct udpeq_channel *chix ;
  struct udpeq_channel *chix2 ;
  struct udpeq_channel **choose ;
  struct sockaddr_in whofrom ;
  int ret, numbytes, fromlen,fromport, buflen ;
  char buf[32768] ;
  char ipaddrs[128] ;
  struct udpeq_packet *packet ;
  struct udpeq_packet *other ;
  struct timeval t1,now ;
  unsigned int magic ;
  long long seq ;
  int next, ix, rix, newfirst, added, do_free, timedout, num_found, expecting_to_add, consecutive_empties, loop_count ;
  long long offset, previous_seq ;
  double delta, delta1, delta2, total_weight, dart, num_channels_chosen, num_channels_desired, prev_iter_num_channels, done ;
  struct pollfd pfd ;
  int num_ro_channels, refcount ;
  char *lates ;
  long long inbuffer_signal_counter_delta ;
  long long inbuffer_signal_timeout_delta ;
  long long inform_packet_count, dropped ;
  char packet_copy[MAX_BUFLEN] ;
  char remote_symbol ;
  char inform_symbol ;
  char inform_local_symbol ;
  char inform_remote_symbol ;
  int delta_usec ;
  int delta_sec ;
  int disbursed ;
  int this_history_slot ;
  int prev_ro_block_history_slot=0 ;
  double drop_rate, best_drop_rate ;
  int inform_choices, inform_ix, found ;

  fprintf(stderr,"channel thread: [%s] (fd=%d)\n",chan->name,chan->fd) ;

  num_ro_channels=0 ;
  for(chix=channel_tail;chix!=NULL;chix=chix->previous)
    if(chix->type==RO)
      num_ro_channels++ ;
  choose=(struct udpeq_channel **)calloc(num_ro_channels,sizeof(struct udpeq_channel *)) ;

  expecting_to_add=0 ;
  for(;;)
    {
      if(!chan->active)
        {
          if(!(chan->master_channel) || (chan->whofrom_init) || (chan->type==LI) || (chan->type==RI))
            {
              ret=open_channel(chan) ;
              if(ret)
                {
                  fprintf(stderr,"failed to open channel [%s]\n",chan->name) ;
                  sleep(7) ;
                }
              else
                {
                  fprintf(stderr,"opened channel [%s]\n",chan->name) ;
                }
            }
          else
            {
              fprintf(stderr,"channel [%s] awaiting whofrom...\n",chan->name) ;
              sleep(8) ;
            }
        }
      else if((chan->type==LI) || (chan->type==RI))
        {
          pfd.fd=chan->fd ;
          pfd.events=POLLIN|POLLERR|POLLHUP|POLLNVAL ;
          pfd.revents=0 ;
          ret=poll(&pfd,1,4800) ;  // wait 4.8 seconds between polling
          if((ret>0)&&(pfd.revents&POLLIN))
            {
              fromlen=sizeof(struct sockaddr_in) ;
              buflen=32768 ;
              numbytes=recvfrom(chan->fd,buf,buflen,MSG_NOSIGNAL,(struct sockaddr *) &whofrom,&fromlen) ;
            }
          else
            {
              if(DEBUG>3)
                {
                  perror("poll") ;
                  fprintf(stderr,"[%s] poll returned %d (flags 0x%X)\n",chan->name,ret,pfd.revents) ;
                }
              if(pfd.revents & (POLLERR|POLLHUP|POLLNVAL))
                {
                  close_channel(chan) ;
                }
              else if(ret>0)
                {
                  usleep(310000) ;
                }
              numbytes=0 ;
            }
          if(numbytes<0)
            {
              if(DEBUG>3)
                {
                  perror("recvfrom") ;
                  fprintf(stderr,"recvfrom failed for [%s]\n",chan->name) ;
                }
              usleep(2500000) ;
            }
          if(numbytes>0)
            {
              gettimeofday(&t1,NULL) ;
              do_free=0 ;

              if(chan->max_recv_size<numbytes)
                chan->max_recv_size=numbytes ;

              chan->packet_count++ ;

              fromport=ntohs(whofrom.sin_port) ;
              sprintf(ipaddrs,"%s",inet_ntoa(whofrom.sin_addr)) ;
              if(DEBUG>6)
                fprintf(stderr,"recvfrom [%s] : %d bytes from %s:%d\n",chan->name,numbytes,ipaddrs,fromport) ;

              if((chan->slave_channel) && (chan->slave_channel->whofrom_init==0))
                {
                  chan->slave_channel->sa.sin_family = AF_INET ;
                  memcpy(&(chan->slave_channel->sa.sin_addr),&(whofrom.sin_addr),sizeof(whofrom.sin_addr)) ;
                  chan->slave_channel->sa.sin_port = whofrom.sin_port ;
                  chan->slave_channel->local_port = fromport ;
                  if(DEBUG>2)
                    fprintf(stderr,"activating slave [%s] on return channel to %s:%d\n",chan->slave_channel->name,ipaddrs,fromport) ;
                  chan->slave_channel->whofrom_init=1 ;
                }

              packet=(struct udpeq_packet *)malloc(sizeof(struct udpeq_packet)) ;
              packet->channel=chan->symbol ;
              memcpy(&(packet->recv_time),&t1,sizeof(struct timeval)) ;

              packet->inbuffer_signal_counter=inbuffer_signal_counter ;
              packet->inbuffer_signal_timeout=inbuffer_signal_timeout ;

              if(chan->type==LI)
                {
                  packet->sequence_number=global_sequence_counter++ ;  // not mutex protected because there is only one LI
                  packet->buflen=numbytes+UDPEQ_HEADER_SIZE ;
                  packet->buf=(char *)malloc(packet->buflen) ;
                  packet->refcount=0 ;
                  refcount=0 ;

                  // prepend header
                  magic=(unsigned int) UDPEQ_DATA_MAGIC ;
                  *((unsigned int *) (packet->buf+(0*sizeof(int))))=magic ;
                  *((time_t *) (packet->buf+(1*sizeof(int))))=t1.tv_sec ;
                  *((int *) (packet->buf+(3*sizeof(int))))=(int) t1.tv_usec ;
                  *((long long *) (packet->buf+(4*sizeof(int))))=packet->sequence_number ;

                  // these are filled in at send time...
                  // *((long long *) (packet->buf+(6*sizeof(int))))=inform_packet_count ;
                  // *((char *) (packet->buf+(8*sizeof(int))))=chan->symbol ;
                  // *((char *) (packet->buf+1+(8*sizeof(int))))=inform_symbol ;
                  // *((char *) (packet->buf+2+(8*sizeof(int))))=inform_remote_symbol ;


                  memcpy(packet->buf+UDPEQ_HEADER_SIZE,buf,numbytes) ;

                  for(num_ro_channels=0;num_ro_channels==0;)
                    {
                      // set up array of RO channels
                      ix=0 ;
                      total_weight=0.0 ;
                      for(chix=channel_tail;chix!=NULL;chix=chix->previous)
                        if(chix->type==RO)
                          {
                            dart=drand48() ;
                            chix->ref_flag=0 ;
                            chix->weight_temp=chix->weight_usage ;
                            if((chix->active) && (chix->outbuffer_len<chix->outbuffer_size) && (chix->weight_temp >= dart))
                              {
                                total_weight+=chix->weight_temp ;
                                choose[ix++]=chix ;
                              }
                          }
                      num_ro_channels=ix ;
                      if(num_ro_channels==0)
                        {
                          if(prev_ro_block_history_slot != this_history_slot)
                            {
                              prev_ro_block_history_slot = this_history_slot ;
                              if(debug_log)
                                fprintf(debug_log,"WARNING: all RO channels are blocked (%d)\n",this_history_slot) ;
                              fprintf(stderr,"WARNING: all RO channels are blocked (%d)\n",this_history_slot) ;
                            }
                          usleep(128000) ;
                        }
                    }
                  num_channels_chosen=0 ;
                  
                  if(0)  // use all channels
                    {
                      for(ix=0;ix<num_ro_channels;ix++)
                        {
                          chix=choose[ix] ;
                          total_weight-=chix->weight_temp ;
                          chix->ref_flag=1 ;
                          refcount++ ;
                          num_channels_chosen++ ;
                        }
                    }
                  else if(1)  // round robin, weighted by capacity
                    {
                      int sufficient_choice=0 ;
                      int best_choice= -1 ;
                      double best_weight=0.0 ;
                      
                      gettimeofday(&now,NULL) ;
                      for(sufficient_choice=0;sufficient_choice<4;sufficient_choice++)
                        {
                          best_choice= -1 ;
                          best_weight=0.0 ;
                          for(ix=0;ix<num_ro_channels;ix++)
                            {
                              chix=choose[ix] ;
                              if(!chix->ref_flag)
                                {
                                  this_history_slot=rotate_history_slots(&now,chix) ;
                                  if((chix->history[this_history_slot].accumulated_weighted_use<best_weight) || (best_choice<0))
                                    {
                                      best_choice=ix ;
                                      best_weight=chix->history[this_history_slot].accumulated_weighted_use ;
                                    }
                                }
                            }
                          if(best_choice>=0)
                            {
                              chix=choose[best_choice] ;
                              if(chix->weight_capacity>0.0)
                                chix->history[this_history_slot].accumulated_weighted_use+=((1.0 + chix->near_drop_rate) / chix->weight_capacity) ;
                              chix->ref_flag=1 ;
                              refcount++ ;
                              num_channels_chosen++ ;
                              if(chix->near_drop_rate<NEAR_DROP_RATE_THRESHOLD)
                                sufficient_choice+=10 ;
                            }
                        }
                    }
                  else if(0)  // choose randomly, weighted by capacity
                    {
                      int best_choice= -1 ;
                      double best_weight=0.0 ;

                      gettimeofday(&now,NULL) ;
                      for(ix=0;ix<num_ro_channels;ix++)
                        {
                          chix=choose[ix] ;
                          this_history_slot=rotate_history_slots(&now,chix) ;
                        }
                      total_weight=0.0 ;
                      for(ix=0;ix<num_ro_channels;ix++)
                        {
                          chix=choose[ix] ;
                          chix->weight_temp=((1.1 - chix->near_drop_rate) * chix->weight_capacity) ;
                          if(chix->weight_temp<0.0)
                            chix->weight_temp=0.0 ;
                          total_weight+=chix->weight_temp ;
                        }
                      dart=drand48()*total_weight ;
                      for(ix=0;(ix<num_ro_channels)&&(dart>=0.0);ix++)
                        {
                          chix=choose[ix] ;
                          dart-=chix->weight_temp ;
                          best_choice=ix ;
                        }
                      if((DEBUG>2) && (lrand48()%RANDOM_INFO_PERIOD==0))
                        {
                          chix=choose[best_choice] ;
                          fprintf(stderr,"CHOOSE RO channel %c (%s) among %d choices, slot %d\n",chix->symbol,chix->name,num_ro_channels,this_history_slot) ;
                          for(ix=0;ix<num_ro_channels;ix++)
                            {
                              chix=choose[ix] ;
                              fprintf(stderr,"  ---: channel %c (%s) weight=%g\n",chix->symbol,chix->name,chix->weight_temp) ;
                            }
                        }
                      if(best_choice>=0)
                        {
                          chix=choose[best_choice] ;
                          chix->ref_flag=1 ;
                          refcount++ ;
                          num_channels_chosen++ ;
                        }
                    }
                  else if(0)  // choose single least crowded channel
                    {
                      long long lowest_byte_count= -1 ;
                      int best_choice=0 ;
                      int num_choices_made=0 ;
                      
                      gettimeofday(&now,NULL) ;

                      for(ix=0;ix<num_ro_channels;ix++)
                        {
                          chix=choose[ix] ;
                          this_history_slot=rotate_history_slots(&now,chix) ;
                        }

                      for(done=0;!done;)
                        {
                          best_choice=0 ;
                          for(ix=0;ix<num_ro_channels;ix++)
                            {
                              chix=choose[ix] ;
                              if(!chix->ref_flag)
                                {
                                  if((lowest_byte_count > chix->history[this_history_slot].byte_count) ||
                                     (lowest_byte_count < 0))
                                    {
                                      best_choice=ix ;
                                      lowest_byte_count = chix->history[this_history_slot].byte_count ;
                                    }
                                }
                            }
                          chix=choose[best_choice] ;
                          total_weight-=chix->weight_temp ;
                          chix->ref_flag=1 ;
                          refcount++ ;
                          num_channels_chosen++ ;

                          if(num_channels_chosen>0)
                            done=1 ;
                        }

                      if(DEBUG>4)
                        {
                          fprintf(stderr,"CHOOSE [%s]\n",chix->name) ;
                        }

                      // if chosen channel is lossy, then also choose a good channel
                      if(1)  // RO channel drop rate data is not available yet
                        {
                          if(chix->near_drop_rate>NEAR_DROP_RATE_THRESHOLD)
                            {
                              if((DEBUG>2) && (lrand48()%RANDOM_INFO_PERIOD==0))
                                {
                                  fprintf(stderr,"CHOOSE [%s] channel is too lossy (%g) (dropped %lld of %lld recent packets at history slot %d)...\n",chix->name,chix->near_drop_rate,chix->near_dropped,chix->near_total,this_history_slot) ;
                                  chix->debug_loss_flag=1 ;
                                }
                              best_drop_rate=chix->near_drop_rate ;
                              for(ix=0;ix<num_ro_channels;ix++)
                                {
                                  chix2=choose[ix] ;
                                  if(!chix2->ref_flag)
                                    {
                                      if(best_drop_rate > chix2->near_drop_rate)
                                        {
                                          best_drop_rate=chix2->near_drop_rate ;
                                          best_choice=ix ;
                                        }
                                    }
                                }
                              chix2=choose[best_choice] ;
                              if(!chix2->ref_flag)
                                {
                                  total_weight-=chix2->weight_temp ;
                                  chix2->ref_flag=1 ;
                                  refcount++ ;
                                  num_channels_chosen++ ;
                                  if((DEBUG>2) && (lrand48()%RANDOM_INFO_PERIOD==0))
                                    {
                                      fprintf(stderr,"CHOOSE [%s] channel is too lossy (%g), also choosing reliable channel [%s] (%g)\n",chix->name,chix->near_drop_rate,chix2->name,best_drop_rate) ;
                                    }
                                }
                            }
                        }
                    }
                  
                  // -------------------------------------------------------
                  
                  assert(refcount>0) ;
                  
                  mutex_take(&refcount_mutex) ;
                  packet->refcount=refcount ;
                  mutex_give(&refcount_mutex) ;
                  
                  for(ix=0;ix<num_ro_channels;ix++)
                    {
                      chix=choose[ix] ;
                      if(chix->ref_flag)
                        {
                          for(disbursed=0;!disbursed;)
                            {
                              mutex_take(&(chix->outbuffer_mutex)) ;
                              if(chix->outbuffer_len<chix->outbuffer_size)
                                {
                                  next=(chix->outbuffer_first+chix->outbuffer_len)%chix->outbuffer_size ;
                                  chix->outbuffer_len++ ;
                                  chix->outbuffer[next]=packet ;
                                  disbursed++ ;
                                }
                              mutex_give(&(chix->outbuffer_mutex)) ;
                              if(disbursed)
                                mutex_give(&(chix->outbuffer_signal)) ;
                              else
                                {
                                  if(debug_log)
                                    fprintf(debug_log,"ERROR: disburse to %c len (%d) is not less than size (%d)\n",chix->symbol,chix->outbuffer_len,chix->outbuffer_size) ;
                                  // assert(0) ;
                                  // RO channel is blocked. sleep...
                                  usleep(280000) ;
                                }
                            }
                        }
                    }
                }
              else if(chan->type==RI)
                {
                  // decode header
                  magic=*((unsigned int *) buf) ;
                  if(magic!=UDPEQ_DATA_MAGIC)
                    {
                      fprintf(stderr,"ERROR: received non-magic packet from [%s]\n",chan->name) ;
                    }
                  else
                    {
                      assert(numbytes>=UDPEQ_HEADER_SIZE) ;
                      packet->buflen=numbytes-UDPEQ_HEADER_SIZE ;
                      packet->buf=(char *)malloc(packet->buflen) ;
                      memcpy(packet->buf,buf+UDPEQ_HEADER_SIZE,packet->buflen) ;

                      packet->send_time.tv_sec=*((time_t *) (buf+(1*sizeof(int)))) ;
                      packet->send_time.tv_usec=(suseconds_t) *((int *) (buf+(3*sizeof(int)))) ;
                      seq=*((long long *) (buf+(4*sizeof(int)))) ;
                      inform_packet_count=*((long long *) (buf+(6*sizeof(int)))) ;
                      remote_symbol=*((char *) (buf+(8*sizeof(int)))) ;
                      inform_symbol=*((char *) (buf+1+(8*sizeof(int)))) ;
                      inform_remote_symbol=*((char *) (buf+2+(8*sizeof(int)))) ;

                      found=0 ;
                      for(chix=channel_tail;(chix!=NULL);chix=chix->previous)
                        if(chix->symbol==inform_symbol)
                          {
                            found=1 ;
                            chix->remote_packet_count=inform_packet_count ;
                            if(chix->remote_symbol!=inform_remote_symbol)
                              {
                                if(chix->remote_symbol != '?')
                                  {
                                    fprintf(stderr,"ERROR! channel %c remote symbol %c changed by inform to %c\n",chix->symbol,chix->remote_symbol,inform_remote_symbol) ;
                                  }
                                chix->remote_symbol=inform_remote_symbol ;
                              }
                          }
                      if((!found)&&(inform_symbol!='-'))
                        {
                          fprintf(stderr,"ERROR: informed of unknown channel (%c) (remote %c)\n",inform_symbol,inform_remote_symbol) ;
                        }
                      else
                        {
                          if(DEBUG>4 && (lrand48()%RANDOM_INFO_PERIOD==0))
                            fprintf(stderr,"INFO: informed of channel (%c) (remote %c) packet count %lld\n",inform_symbol,inform_remote_symbol,inform_packet_count) ;
                        }

                      delta_usec=((packet->recv_time.tv_sec-packet->send_time.tv_sec)*1000000) + (packet->recv_time.tv_usec-packet->send_time.tv_usec) ;
                      if((chan->min_network_delta_usec>delta_usec)||(chan->remote_symbol!=remote_symbol))
                        {
                          chan->min_network_delta_usec=delta_usec ;
                          chan->remote_symbol=remote_symbol ;
                        }
                      else
                        {
                          delta_usec=chan->min_network_delta_usec ;
                        }
                      delta_sec=(delta_usec / 1000000) ;
                      delta_usec=(delta_usec % 1000000) ;
                      packet->send_time.tv_sec+=delta_sec ;
                      packet->send_time.tv_usec+=delta_usec ;
                      for(;packet->send_time.tv_usec<0;)
                        {
                          packet->send_time.tv_sec-=1 ;
                          packet->send_time.tv_usec+=1000000 ;
                        }
                      for(;packet->send_time.tv_usec>=1000000;)
                        {
                          packet->send_time.tv_sec+=1 ;
                          packet->send_time.tv_usec-=1000000 ;
                        }

                      packet->sequence_number=seq ;

                      for(added=0;!added;)
                        {
                          loop_count=0 ;
                          consecutive_empties=0 ;
                          // wait for buffer space
                          for(;seq>=inbuffer_sequence+inbuffer_size;)
                            {
                              if(((loop_count%12)==0)&&(DEBUG>2))
                                {
                                  fprintf(stderr,"[%s] : waiting for buffer space for sequence %lld (ibs=%lld, last=%lld, pres=%lld)\n",chan->name,seq,inbuffer_sequence,inbuffer_last,previous_seq) ;
                                }
                              loop_count++ ;
                              if((inbuffer_sequence>=inbuffer_last)&&(previous_seq==inbuffer_sequence))
                                {
                                  consecutive_empties++ ;
                                }
                              else
                                {
                                  previous_seq=inbuffer_sequence ;
                                  consecutive_empties=0 ;
                                }
                              if(consecutive_empties>12)
                                {
                                  fprintf(stderr,"[%s] : resetting sequence from %lld to %lld\n",chan->name,previous_seq,seq) ;
                                  inbuffer_sequence=seq ;
                                  inbuffer_last=seq-1 ;
                                }
                              else
                                {
                                  usleep(256000) ;
                                }
                            }
                          newfirst=0 ;
                          mutex_take(&inbuffer_mutex) ;
                          if(DEBUG>7)
                            fprintf(stderr,"[%s] : (A) took inbuffer mutex\n",chan->name) ;
                          offset=seq-inbuffer_sequence ;
                          if(offset<0)
                            {
                              if(inbuffer_sequence>=inbuffer_last)  // empty inbuffer
                                {
                                  gettimeofday(&now,NULL) ;
                                  delta=((double) (now.tv_sec-last_LO_send_time.tv_sec)) + (((double) (now.tv_usec-last_LO_send_time.tv_usec)) / 1000000.0) ;
                                  if(delta > 8.0)
                                    {
                                      chan->num_jumpstarts++ ;

                                      this_history_slot=rotate_history_slots(&now,chan) ;
                                      chan->history[this_history_slot].num_jumpstarts++ ;

                                      fprintf(stderr,"[%s] : empty inbuffer: resetting sequence from %lld to %lld after %g seconds of inactivity on LO\n",chan->name,previous_seq,seq,delta) ;
                                      inbuffer_sequence=seq ;
                                      inbuffer_last=seq-1 ;
                                      offset=0 ;

                                      reset_latebuffer(seq) ;
                                    }
                                }
                            }
                          if(offset<0)  // packet is too late
                            {
                              ret=update_latebuffer(seq) ;
                              if(ret)
                                {
                                  if(DEBUG>4)
                                    fprintf(stderr,"[%s] : drop too-old packet sequence %lld (late=%d)\n",chan->name,seq,ret) ;
                                  added=1 ;
                                  do_free=1 ;
                                }
                              else
                                {
                                  // add packet to doubly-linked-list
                                  packet->newer=NULL ;
                                  packet->older=NULL ;
                                  mutex_take(&latebuffer_mutex) ;
                                  if(latebuffer_newest==NULL)
                                    {
                                      assert(latebuffer_oldest==NULL) ;
                                      latebuffer_newest=packet ;
                                      latebuffer_oldest=packet ;
                                    }
                                  else
                                    {
                                      assert(latebuffer_oldest!=NULL) ;
                                      packet->older=latebuffer_newest ;
                                      latebuffer_newest->newer=packet ;
                                      latebuffer_newest=packet ;
                                    }
                                  mutex_give(&latebuffer_mutex) ;
                                  newfirst=1 ;
                                  added=1 ;
                                }
                            }
                          else if(offset<inbuffer_size)
                            {
                              if(inbuffer_last < packet->sequence_number)
                                inbuffer_last = packet->sequence_number ;
                              added=1 ;
                              ix=(inbuffer_first+offset)%inbuffer_size ;
                              other=inbuffer[ix] ;
                              if(other)
                                {
                                  if(other->sequence_number == packet->sequence_number)
                                    {
                                      // ignore duplicate packet
                                      if(DEBUG>4)
                                        fprintf(stderr,"[%s] : ignore duplicate packet sequence %lld\n",chan->name,seq) ;
                                      do_free=1 ;
                                    }
                                  else
                                    {
                                      // replace older packet
                                      inbuffer[ix]=packet ;
                                      packet=other ;
                                      do_free=1 ;
                                      if(offset==0)
                                        newfirst=1 ;
                                      ret=update_latebuffer(seq) ;  // mark position in latebuffer to avoid duplicates
                                    }
                                }
                              else
                                {
                                  inbuffer[ix]=packet ;
                                  if(offset==0)
                                    newfirst=1 ;
                                  ret=update_latebuffer(seq) ;  // mark position in latebuffer to avoid duplicates
                                }
                            }
                          else
                            {
                              fprintf(stderr,"ERROR CASE 42 [%s]\n",chan->name) ;
                              assert(0) ;
                            }
                          if(DEBUG>7)
                            fprintf(stderr,"[%s] : (A) giving inbuffer mutex\n",chan->name) ;
                          mutex_give(&inbuffer_mutex) ;
                        }
                      if(newfirst)
                        {
                          mutex_give(&inbuffer_signal) ;
                          inbuffer_signal_counter++ ;
                        }
                    }
                }
              else
                {
                  fprintf(stderr,"ERROR CASE 43 [%s]\n",chan->name) ;
                }

              // we are in the LI or RI case ...
              update_history_slot(chan,packet) ;

              if(do_free)
                {
                  if(packet->buf)
                    free(packet->buf) ;
                  if(packet)
                    free(packet) ;
                  packet=NULL ;
                }
            }
        }
      else if(chan->type==LO)
        {
          lates="unknown" ;
          // mutex_take(&inbuffer_signal) ;
          if(inbuffer[inbuffer_first])
            timedout=0 ;
          else
            timedout=mutex_take_timed(&inbuffer_signal,MAX_WAIT_FOR_MISSING_PACKET_USEC) ;
          if(timedout)
            inbuffer_signal_timeout++ ;
          num_found=0 ;
          for(added=1;added;)
            {
              packet=NULL ;
              if(latebuffer_oldest)
                {
                  mutex_take(&latebuffer_mutex) ;
                  packet=latebuffer_oldest ;
                  if(packet)
                    {
                      lates="late" ;
                      if(latebuffer_newest==packet)
                        {
                          latebuffer_newest=NULL ;
                          latebuffer_oldest=NULL ;
                        }
                      else
                        {
                          latebuffer_oldest=packet->newer ;
                          latebuffer_oldest->older=NULL ;
                        }
                    }
                  mutex_give(&latebuffer_mutex) ;
                }
              if(!packet)
                {
                  mutex_take(&inbuffer_mutex) ;
                  if(DEBUG>7)
                    fprintf(stderr,"[%s] : (B) took inbuffer mutex\n",chan->name) ;
                  packet=inbuffer[inbuffer_first] ;
                  if((!packet) && timedout)
                    {
                      if(expecting_to_add && (num_found==0))
                        {
                          // if we are losing packets, skip ahead to next available sequence
                          for(;(!packet)&&(inbuffer_sequence < inbuffer_last);)
                            {
                              inbuffer_first=(inbuffer_first + 1)%inbuffer_size ;
                              packet=inbuffer[inbuffer_first] ;
                              inbuffer_sequence++ ;
                            }

                          if((DEBUG>2) && (lrand48()%RANDOM_INFO_PERIOD==0))
                            fprintf(stderr,"** TIMEDOUT ** [%s] expecting_to_add=%d num_found=%d, packet=%p\n",chan->name,expecting_to_add,num_found,packet) ;

                          expecting_to_add=0 ;
                          if(packet)
                            {
                              chan->num_jumpstarts++ ;
                              gettimeofday(&now,NULL) ;
                              this_history_slot=rotate_history_slots(&now,chan) ;
                              chan->history[this_history_slot].num_jumpstarts++ ;
                            }
                        }
                    }
                  if(packet)
                    {
                      lates="soon" ;
                      inbuffer[inbuffer_first]=NULL ;
                      inbuffer_sequence=packet->sequence_number + 1 ;
                      inbuffer_first=(inbuffer_first + 1)%inbuffer_size ;
                    }
                  
                  if(DEBUG>7)
                    fprintf(stderr,"[%s] : (B) giving inbuffer mutex\n",chan->name) ;
                  mutex_give(&inbuffer_mutex) ;
                }
              if(packet)
                {
                  num_found++ ;
                  if(packet->buf)
                    {
                      ret=send(chan->fd,packet->buf,packet->buflen,0) ;
                      if(ret<packet->buflen)
                        {
                          perror("send") ;
                          fprintf(stderr,"ERROR: [%s] sent only %d of %d bytes to [%s]\n",chan->name,ret,packet->buflen,chan->destname) ;
                        }
                      else
                        {
                          if(chan->max_send_size<ret)
                            chan->max_send_size=ret ;

                          if(debug_log)
                            {
                              gettimeofday(&now,NULL) ;
                              inbuffer_signal_counter_delta=inbuffer_signal_counter - packet->inbuffer_signal_counter ;
                              inbuffer_signal_timeout_delta=inbuffer_signal_timeout - packet->inbuffer_signal_timeout ;

                              delta=((double) (now.tv_sec-packet->send_time.tv_sec)) + (((double) (now.tv_usec-packet->send_time.tv_usec)) / 1000000.0) ;
                              delta1=((double) (packet->recv_time.tv_sec-packet->send_time.tv_sec)) + (((double) (packet->recv_time.tv_usec-packet->send_time.tv_usec)) / 1000000.0) ;
                              delta2=((double) (now.tv_sec-packet->recv_time.tv_sec)) + (((double) (now.tv_usec-packet->recv_time.tv_usec)) / 1000000.0) ;

                              fprintf(debug_log,"LO:%c (%s) %lld\t%2.8lf\t%2.8lf\t%2.8lf\tdc%lld,dt%lld\n",packet->channel,lates,packet->sequence_number,delta,delta1,delta2,inbuffer_signal_counter_delta,inbuffer_signal_timeout_delta) ;  // TIME: overall, net, buffer
                              debug_lines++ ;
                              if(debug_lines>8000000)
                                {
                                  debug_lines=0 ;
                                  debug_log = freopen(NULL, "w+", debug_log) ;
                                }

                            }
                          if(DEBUG>6)
                            fprintf(stderr,"[%s] sent %d bytes [seq %lld] to [%s]\n",chan->name,ret,packet->sequence_number,chan->destname) ;
                          chan->packet_count++ ;
                        }
                      free(packet->buf) ;
                    }
                  free(packet) ;
                }
              else
                {
                  added=0 ;
                }
            }
          if(num_found>0)
            {
              gettimeofday(&last_LO_send_time,NULL) ;
            }

          if(timedout && (num_found==0) && (inbuffer_sequence <= inbuffer_last))
            {
              expecting_to_add=1 ;
            }
          else if(!timedout)
            {
              if(num_found>0)
                expecting_to_add=0 ;
              else
                expecting_to_add=1 ;
            }
        }
      else if(chan->type==RO)
        {
          mutex_take(&(chan->outbuffer_signal)) ;
          for(added=1;added && (chan->outbuffer_len>0);)
            {
              mutex_take(&(chan->outbuffer_mutex)) ;
              packet=chan->outbuffer[chan->outbuffer_first] ;
              if(packet)
                {
                  chan->outbuffer[chan->outbuffer_first]=NULL ;
                  chan->outbuffer_first=(chan->outbuffer_first + 1)%chan->outbuffer_size ;
                  chan->outbuffer_len-- ;
                }
              mutex_give(&(chan->outbuffer_mutex)) ;
              if(packet)
                {
                  assert(packet->buflen < MAX_BUFLEN) ;

                  // inform random channel
                  inform_symbol='-' ;
                  inform_ix= -1 ;
                  inform_local_symbol='-' ;
                  inform_packet_count=0 ;
                  inform_choices=0 ;
                  for(chix=channel_tail;chix!=NULL;chix=chix->previous)
                    if((chix->active)&&(chix->remote_symbol!='?'))
                      inform_choices++ ;
                  if(inform_choices>0)
                    {
                      ix=lrand48()%inform_choices ;
                      inform_ix=ix ;
                      for(chix=channel_tail;(chix!=NULL)&&(ix>=0);chix=chix->previous)
                        if((chix->active)&&(chix->remote_symbol!='?'))
                          {
                            if(ix==0)
                              {
                                inform_local_symbol=chix->symbol ;
                                inform_symbol=chix->remote_symbol ;
                                inform_packet_count=chix->packet_count ;
                              }
                            ix-- ;
                          }
                    }

                  if(1)
                    {
                      memcpy(packet_copy,packet->buf,packet->buflen) ;
                      *((long long *) (packet_copy+(6*sizeof(int))))=inform_packet_count ;
                      *((char *) (packet_copy+(8*sizeof(int))))=chan->symbol ;
                      *((char *) (packet_copy+1+(8*sizeof(int))))=inform_symbol ;
                      *((char *) (packet_copy+2+(8*sizeof(int))))=inform_local_symbol ;

                      ret=send(chan->fd,packet_copy,packet->buflen,0) ;
                    }
                  else  // old way, send packet with no per-channel data
                    {
                      ret=send(chan->fd,packet->buf,packet->buflen,0) ;
                    }

                  gettimeofday(&now,NULL) ; // used in multiple cases below...

                  if(ret<packet->buflen)
                    {
                      perror("send") ;
                      switch(errno)
                        {
                        case ENOTCONN :
                        case EDESTADDRREQ :
                          if(DEBUG>5)
                            fprintf(stderr,"\nmarking channel %c inactive  ",chan->symbol) ;
                          chan->active=0 ;
                          chan->whofrom_init=0 ;
                          break ;;;
                        }
                      fprintf(stderr,"ERROR: [%s] sent only %d of %d bytes to [%s]\n",chan->name,ret,packet->buflen,chan->destname) ;
                    }
                  else
                    {
                      if(chan->max_send_size<ret)
                        chan->max_send_size=ret ;
                      if(debug_log)
                        {
                          delta2=((double) (now.tv_sec-packet->recv_time.tv_sec)) + (((double) (now.tv_usec-packet->recv_time.tv_usec)) / 1000000.0) ;
                          fprintf(debug_log,"RO:%c %lld\t%2.8lf\n",chan->symbol,packet->sequence_number,delta2) ;
                          debug_lines++ ;
                        }

                      if(DEBUG>6)
                        fprintf(stderr,"[%s] sent %d bytes [seq %lld] to [%s]\n",chan->name,ret,packet->sequence_number,chan->destname) ;
                      chan->packet_count++ ;

                      update_history_slot(chan,packet) ;
                    }
                  do_free=0 ;
                  mutex_take(&refcount_mutex) ;
                  packet->refcount-- ;
                  if(packet->refcount<1)
                    do_free=1 ;
                  mutex_give(&refcount_mutex) ;
                  if(do_free)
                    {
                      if(packet->buf)
                        free(packet->buf) ;
                      if(packet)
                        free(packet) ;
                      packet=NULL ;
                    }
                }
              else
                {
                  added=0 ;
                }
            }
        }
      else
        {
          fprintf(stderr,"ERROR CASE 44 [%s]\n",chan->name) ;
          sleep(2) ;
        }
    }
  return(NULL) ;
}

void *udpeq_channel_thread_wrapper(void *arg)
{
  void *result=NULL ;
  char *oldname=NULL ;
  struct udpeq_channel *chan=(struct udpeq_channel *) arg ;

  oldname=chan->name ;
  chan->name=malloc(512) ;
  if(oldname)
    sprintf(chan->name,"(%c)%s:%s",chan->symbol,name_of_type(chan,0,1),oldname) ;
  else
    sprintf(chan->name,"(%c)%s:%s:%d",chan->symbol,name_of_type(chan,0,0),chan->hostname,chan->port) ;

  result=udpeq_channel_thread(chan->name, chan) ;

  return(result) ;
}

void udpeq()
{
  struct udpeq_channel *chan ;
  char details[STRLEN] ;
  int ret ;
  double kbps ;
  struct timeval now ;
  int this_history_slot ;
  int previous_history_slot ;

  for(chan=channel_tail;chan!=NULL;chan=chan->previous)
    {
      ret=pthread_create(&(chan->channel_thread), NULL, &udpeq_channel_thread_wrapper, (void *) chan) ;
      if(ret)
        {
          perror("pthread_create") ;
          assert(0) ;
        }
    }

  for(;;)
    {
      fprintf(stderr,"udpeq%s------------------------------------(ibsc=%lld,ibst=%lld)\n",UDPEQ_VERSION,inbuffer_signal_counter,inbuffer_signal_timeout) ;

      mutex_take(&inbuffer_mutex) ;
      mutex_give(&inbuffer_mutex) ;
      gettimeofday(&now,NULL) ;

      this_history_slot=rotate_history_slots(&now,NULL) ;
      previous_history_slot=(this_history_slot + NUM_HISTORY_SLOTS - 1) % NUM_HISTORY_SLOTS ;
      for(chan=channel_tail;chan!=NULL;chan=chan->previous)
        {
          details[0]=0 ;
          if(chan->type==RO)
            {
              sprintf(details," {%c}",chan->remote_symbol) ;
              // sprintf(details," obfirst=%d,len=%d,[%d{%lld..%lld}]#p%lld,#b%lld",chan->outbuffer_first,chan->outbuffer_len,this_history_slot,chan->history[this_history_slot].min_packet_number,chan->history[this_history_slot].max_packet_number,chan->history[this_history_slot].packet_count,chan->history[this_history_slot].byte_count) ;
              if(telemetry_log)
                {
                  fprintf(telemetry_log,"udpeq.%s.packet_count %lld %ld.%06ld\n",chan->name,chan->history[previous_history_slot].packet_count,now.tv_sec,now.tv_usec) ;
                  fprintf(telemetry_log,"udpeq.%s.byte_count %lld %ld.%06ld\n",chan->name,chan->history[previous_history_slot].byte_count,now.tv_sec,now.tv_usec) ;
                }
            }
          else if(chan->type==LO)
            {
              sprintf(details," inbuf_first=%d,seq=%lld,last=%lld",inbuffer_first,inbuffer_sequence,inbuffer_last) ;
            }
          else if(chan->type==LI)
            {
              sprintf(details," seq=%lld",global_sequence_counter) ;
            }
          else if(chan->type==RI)
            {
              sprintf(details," {%c}",chan->remote_symbol) ;
            }

          fprintf(stderr,"##--A%dfd%dw%gW%g[%s](count %lld, remote count %lld, jumps %lld, (near/far) drop rate %g/%g)(mxsnd=%d, mxrcv=%d)%s\n",chan->active,chan->fd,chan->weight_usage,chan->weight_capacity,chan->name,chan->packet_count,chan->remote_packet_count,chan->num_jumpstarts,chan->near_drop_rate,chan->far_drop_rate,chan->max_send_size,chan->max_recv_size,details) ;

          // telemetry common to all channel types
          if(telemetry_log)
            {
              kbps=(((double) chan->history[previous_history_slot].byte_count)/128.0)/((double) HISTORY_SLOT_DURATION_SECS) ;
              fprintf(telemetry_log,"udpeq.%s.kbps %g %ld.%06ld\n",chan->name,kbps,now.tv_sec,now.tv_usec) ;
              fprintf(telemetry_log,"udpeq.%s.jumpstarts %lld %ld.%06ld\n",chan->name,chan->history[previous_history_slot].num_jumpstarts,now.tv_sec,now.tv_usec) ;
              if((chan->type!=LI)&&(chan->type!=LO))
                {
                  fprintf(telemetry_log,"udpeq.%s.active %d %ld.%06ld\n",chan->name,chan->active,now.tv_sec,now.tv_usec) ;
                  fprintf(telemetry_log,"udpeq.%s.near_drop_rate %g %ld.%06ld\n",chan->name,chan->near_drop_rate,now.tv_sec,now.tv_usec) ;
                  fprintf(telemetry_log,"udpeq.%s.far_drop_rate %g %ld.%06ld\n",chan->name,chan->far_drop_rate,now.tv_sec,now.tv_usec) ;
                  fprintf(telemetry_log,"udpeq.%s.weight_capacity %g %ld.%06ld\n",chan->name,chan->weight_capacity,now.tv_sec,now.tv_usec) ;
                  // fprintf(telemetry_log,"udpeq.%s.packet_count %lld %ld.%06ld\n",chan->name,chan->packet_count,now.tv_sec,now.tv_usec) ;
                  // fprintf(telemetry_log,"udpeq.%s.remote_packet_count %lld %ld.%06ld\n",chan->name,chan->remote_packet_count,now.tv_sec,now.tv_usec) ;
                }
            }
        }
      fflush(telemetry_log) ;
      sleep(8) ;
    }

  for(chan=channel_tail;chan!=NULL;chan=chan->previous)
    {
      ret=pthread_join(chan->channel_thread, NULL) ;
    }
}

int main(int argc, char **argv)
{
  int result=0 ;
  struct timeval now ;
  int ret ;

  fprintf(stderr,"udpeq version %s\n",UDPEQ_VERSION) ;

  assert(sizeof(int)==4) ;
  assert(sizeof(time_t)==8) ;
  assert(sizeof(double)==8) ;
  assert(sizeof(long long)==8) ;
  assert(NUM_HISTORY_SLOTS_FOR_NEAR_DROP_RATE < NUM_HISTORY_SLOTS) ;
  assert(NUM_HISTORY_SLOTS_FOR_FAR_DROP_RATE < NUM_HISTORY_SLOTS) ;

  setlinebuf(stdout) ;

  ret=sem_init(&refcount_mutex, 0, 1) ;
  ret=sem_init(&history_slot_mutex, 0, 1) ;

  ret=sem_init(&inbuffer_mutex, 0, 1) ;
  ret=sem_init(&inbuffer_signal, 0, 1) ;
  inbuffer_size=ASSEMBLY_BUFFER_SIZE ;
  inbuffer=(struct udpeq_packet **)calloc(inbuffer_size,sizeof(struct udpeq_packet *)) ;
  inbuffer_first=0 ;
  inbuffer_sequence=0 ;

  ret=sem_init(&latebuffer_mutex, 0, 1) ;
  latebufferA=(long long *)calloc(LATE_BUFFER_SIZE,sizeof(long long)) ;
  latebufferB=(long long *)calloc(LATE_BUFFER_SIZE,sizeof(long long)) ;



  gettimeofday(&now,NULL) ;
  srandom(((long) getpid())*now.tv_usec + (now.tv_sec^((long) &now))) ;
  srand48((long) random()) ;
  channel_tail=parse_command_line(argc,argv) ;

  fprintf(stderr,"using verbosity level %d\n",DEBUG) ;

  if(channel_tail)
    udpeq() ;
  else
    {
      usage(argv[0]) ;
      result= -1 ;
    }

  return(result) ;
}
